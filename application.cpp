/* RAAT Includes */

#include "raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "blinker.hpp"

/* Defines, typedefs */

typedef enum _eState
{
    eState_WaitingForStart,
    eState_Blink0,
    eState_Blink0Delay,
    eState_Blink1,
    eState_Blink1Delay,
    eState_Blink2,
    eState_Blink2Delay,
    eState_Blink3,
    eState_BlinkAll,
    eState_PostBlinkDelay,
    eState_Finished
} eState;

typedef enum _eStartSource
{
    eStartSource_Button,
    eStartSource_Switch
} eStartSource;

typedef struct
{
    eState State;
    eStartSource StartSource;
} ApplicationData;

/* Local Variables */

static uint8_t s_colours[4][3] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}
};

static uint8_t blink_count[4];

static const raat_devices_struct * s_pDevices = NULL;

static ApplicationData AppData = {eState_WaitingForStart, eStartSource_Button};

static RAATOneShotTimer DelayTimer(0);

/* Static Functions */

static void set_pair(uint8_t pair, bool status)
{
    const uint8_t led1 = pair * 2;
    const uint8_t led2 = led1 + 1;

    raat_logln_P(LOG_APP, PSTR("Blinker %d %s"), pair, status ? "on" : "off");

    if (status)
    {
        s_pDevices->pNeoPixels->setPixelColor(led1, s_colours[pair][0], s_colours[pair][1], s_colours[pair][2]);
        s_pDevices->pNeoPixels->setPixelColor(led2, s_colours[pair][0], s_colours[pair][1], s_colours[pair][2]);
    }
    else
    {
        s_pDevices->pNeoPixels->setPixelColor(led1, 0, 0, 0);
        s_pDevices->pNeoPixels->setPixelColor(led2, 0, 0, 0);
    }    
    s_pDevices->pNeoPixels->show();
}
static void on_blink_change_cb1(bool status)
{
    set_pair(0, status);
}

static void on_blink_change_cb2(bool status)
{
    set_pair(1, status);
}

static void on_blink_change_cb3(bool status)
{
    set_pair(2, status);
}

static void on_blink_change_cb4(bool status)
{
    set_pair(3, status);
}

static Blinker blinkers[4] = {
    Blinker(250, 250, on_blink_change_cb1),
    Blinker(250, 250, on_blink_change_cb2),
    Blinker(250, 250, on_blink_change_cb3),
    Blinker(250, 250, on_blink_change_cb4)
};

static void play_intro(const raat_devices_struct& devices)
{
    devices.pNeoPixels->set_pixels(0, 7, 255, 0 ,0);
    devices.pNeoPixels->show();
    delay(500);

    devices.pNeoPixels->set_pixels(0, 7, 0, 255 ,0);
    devices.pNeoPixels->show();
    delay(500);

    devices.pNeoPixels->set_pixels(0, 7, 0, 0, 255);
    devices.pNeoPixels->show();
    delay(500);

    devices.pNeoPixels->set_pixels(0, 7, 0, 0, 0);
    devices.pNeoPixels->show();
}

static void get_combination(const raat_params_struct& params, uint8_t * pCounts)
{
    char combination[5];
    params.pCombination->get(combination);
    pCounts[0] = combination[0] - '0';
    pCounts[1] = combination[1] - '0';
    pCounts[2] = combination[2] - '0';
    pCounts[3] = combination[3] - '0';
}

static void setup_blinkers(const raat_devices_struct& devices, const raat_params_struct& params)
{
    get_combination(params, blink_count);
    for (uint8_t i=0; i<4; i++)
    {
        params.pColours[i]->get(s_colours[i]);
    }
}

static void start_blinker(uint8_t i, const raat_devices_struct& devices, const raat_params_struct& params)
{
    raat_logln_P(LOG_APP, PSTR("Starting blinker %d (%d blinks, %" PRIu8 ", %" PRIu8 ", %" PRIu8 ")"),
        i, blink_count[i],
        s_colours[i][0], s_colours[i][1], s_colours[i][2]
    );
    blinkers[i].set_timings(params.pShort_Delay->get(), params.pLong_Delay->get());
    blinkers[i].start(blink_count[i]);
}

static void start_together(const raat_devices_struct& devices, const raat_params_struct& params)
{
    for (uint8_t i=0; i<4; i++)
    {
        start_blinker(i, devices, params);
    }
}

static bool check_for_start(
    const raat_devices_struct& devices, const raat_params_struct& params,
    ApplicationData& appData
)
{
    bool start_button = devices.pStartButton->check_high_and_clear();
    bool start_switch = devices.pStartSwitch->state() == true;

    if (start_button || start_switch)
    {
        if (start_button)
        {
            appData.StartSource = eStartSource_Button;
        }
        else if (start_switch)
        {
            appData.StartSource = eStartSource_Switch;   
        }
    }
    return start_button || start_switch;
}

static void start_blinking(const raat_devices_struct& devices, const raat_params_struct& params,
    ApplicationData& appData)
{
    bool blink_together = devices.pModeSelect->state() == true;

    setup_blinkers(devices, params);
    if (blink_together)
    {
        raat_logln_P(LOG_APP, PSTR("Starting together blinks"));
        start_together(devices, params);
        AppData.State = eState_BlinkAll;
    }
    else
    {
        raat_logln_P(LOG_APP, PSTR("Starting sequential blinks"));
        start_blinker(0, devices, params);
        AppData.State = eState_Blink0;
    }
}

/* Public Functions */

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    s_pDevices = &devices;
    play_intro(devices);
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    switch (AppData.State)
    {
    case eState_WaitingForStart:
        if (check_for_start(devices, params, AppData))
        {
            start_blinking(devices, params, AppData);
        }
        break;

    case eState_Blink0:
        if (!blinkers[0].running())
        {
            start_blinker(1, devices, params);
            AppData.State = eState_Blink0Delay;
            DelayTimer.start(params.pInter_Blink_Delay->get());
        }
        break;
    case eState_Blink0Delay:
        if (DelayTimer.check_and_reset())
        {
            AppData.State = eState_Blink1;
        }
        break;

    case eState_Blink1:
        if (!blinkers[1].running())
        {
            start_blinker(2, devices, params);
            AppData.State = eState_Blink1Delay;
            DelayTimer.start(params.pInter_Blink_Delay->get());
        }
        break;
    case eState_Blink1Delay:
        if (DelayTimer.check_and_reset())
        {
            AppData.State = eState_Blink2;
        }
        break;

    case eState_Blink2:
        if (!blinkers[2].running())
        {
            start_blinker(3, devices, params);
            AppData.State = eState_Blink2Delay;
            DelayTimer.start(params.pInter_Blink_Delay->get());
        }
        break;
    case eState_Blink2Delay:
        if (DelayTimer.check_and_reset())
        {
            AppData.State = eState_Blink3;
        }
        break;

    case eState_Blink3:
        if (!blinkers[3].running())
        {
            AppData.State = eState_PostBlinkDelay;
            raat_logln_P(LOG_APP, PSTR("Blinking done"));
            DelayTimer.start(params.pPost_Blink_Delay->get());
        }
        break;

    case eState_BlinkAll:
        if (!blinkers[0].running() && !blinkers[1].running() && !blinkers[2].running() && !blinkers[3].running())
        {
            AppData.State = eState_PostBlinkDelay;
            raat_logln_P(LOG_APP, PSTR("Blinking done"));
            DelayTimer.start(params.pPost_Blink_Delay->get());
        }
        break;

    case eState_PostBlinkDelay:
        if (DelayTimer.check_and_reset())
        {
            AppData.State = eState_Finished;
        }
        break;

    case eState_Finished:
        switch(AppData.StartSource)
        {
        case eStartSource_Button:
            start_blinking(devices, params, AppData);
            break;
        case eStartSource_Switch:
            if (devices.pStartSwitch->state() == true)
            {
                start_blinking(devices, params, AppData);
            }
            break;
        }
    }

    blinkers[0].update();
    blinkers[1].update();
    blinkers[2].update();
    blinkers[3].update();
}
